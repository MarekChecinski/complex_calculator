# simple complex number calculator
# Input data section
# re_num1 = float(input("Enter first number(Re): "))
# im_num1 = float(input("Enter first number(Im): "))
# re_num2 = float(input("Enter second number(Re):  "))
# im_num2 = float(input("Enter second number(Im):  "))
#
# # show on the screen inserted data
# number_1 = [re_num1, im_num1]
# number_2 = [re_num2, im_num2]
# print("First number: ")
# print(str(number_1[0]) + " + " + str(number_1[1]) + "i")
# print("Second number: ")
# print(str(number_2[0]) + " + " + str(number_2[1]) + "i")


class number:
    def __init__(self, re, im):
        self.re = re
        self.im = im
    def show(self):
        c = ''
        if self.im > 0:
            c = '+'
        else:
            c = '-'
        return f"{self.re} {c} {abs(self.im)}i"

#
# # fuction section
# def disp(result):
#     print("Result: ")
#     print(str(result[0]) + " + " + str(result[1]) + "i")
#     return 0
#
#
def add(num1: number, num2: number):
    re = num1.re + num2.re
    im = num1.im + num2.im
    result = number(re, im)
    return result
#
#
# def sub(num1, num2):
#     re = num1[0] - num2[0]
#     im = num1[1] - num2[1]
#     result = [re, im]
#     return result
#
#
# def mul(num1, num2):
#     re = (num1[0] * num2[0]) + (num1[1] * num2[1]) * (-1)
#     im = (num1[0] * num2[1]) + (num2[0] * num1[1])
#     result = [re, im]
#     return result
#
#
# def div(num1, num2):
#     re = (num1[0] * num2[0] + (num1[1] * num2[1])) / (num2[0] * num2[0] + (num2[1] * num2[1]))
#     im = ((-1) * (num1[0] * num2[1]) + num1[1] * num2[0]) / (num2[0] * num2[0] + (num2[1] * num2[1]))
#     result = [re, im]
#     return result
#
#
# # mathematical operation choose
# print("Choose mathematical operation: \n 1.Addition \n 2.Subtraction \n 3.Multiplication \n 4.Division \n")
# val = int(input("Enter value: \n"))
# if val == 1:
#     disp(add(number_1, number_2))
# elif val == 2:
#     disp(sub(number_1, number_2))
# elif val == 3:
#     disp(mul(number_1, number_2))
# elif val == 4:
#     disp(div(number_1, number_2))
# else:
#     print("Wrong number!!!")


def main():
    print('OK')
    num1 = number(1, 1)
    num2 = number(-2, -2)
    print(add(num1, num2).show())

if __name__ == '__main__':
    main()